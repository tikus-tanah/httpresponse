package httpresponse

import (
	"encoding/json"
	"testing"
)

func TestMakeStruct(t *testing.T) {
	fs := []Field{
		{
			Name:  "message",
			Value: "Hello World",
		},
	}

	resp := makeStruct(fs)
	jsonbs, err := json.Marshal(resp)
	if err != nil {
		t.Fatal(err.Error())
	}

	t.Logf("%s", jsonbs)
}

func TestMakeStructNilInterface(t *testing.T) {
	fs := []Field{
		{
			Name:  "message",
			Value: nil,
		},
	}

	resp := makeStruct(fs)
	jsonbs, err := json.Marshal(resp)
	if err != nil {
		t.Fatal(err.Error())
	}

	t.Logf("%s", jsonbs)
}

func TestMakeStructNilValue(t *testing.T) {
	var val []string
	val = nil
	fs := []Field{
		{
			Name:  "message",
			Value: val,
		},
	}

	resp := makeStruct(fs)
	jsonbs, err := json.Marshal(resp)
	if err != nil {
		t.Fatal(err.Error())
	}

	t.Logf("%s", jsonbs)
}

func TestMakeStructInterface(t *testing.T) {
	var val interface{}
	val = "Hello World!"
	fs := []Field{
		{
			Name:  "message",
			Value: val,
		},
	}

	resp := makeStruct(fs)
	jsonbs, err := json.Marshal(resp)
	if err != nil {
		t.Fatal(err.Error())
	}

	t.Logf("%s", jsonbs)
}

func TestMakeResponse(t *testing.T) {
	resp := MakeResponse(StatusOK,
		Field{
			Name:  "val1",
			Value: "value 1",
		},
		Field{
			Name:  "val2",
			Value: "value 2",
		},
	)

	jsonbs, err := json.Marshal(resp)
	if err != nil {
		t.Fatal(err.Error())
	}

	t.Logf("%s", jsonbs)
}

func TestOk(t *testing.T) {
	resp := OK("Hello World", "message")
	jsonbs, err := json.Marshal(resp)
	if err != nil {
		t.Error(err.Error())
		return
	}

	t.Logf("%s", jsonbs)
}

func TestBadRequest(t *testing.T) {
	resp := BadRequest("invalid_password", "Invalid password")
	jsonbs, err := json.Marshal(resp)
	if err != nil {
		t.Error(err.Error())
		return
	}

	t.Logf("%s", jsonbs)
}

func TestFound(t *testing.T) {
	resp := Found()
	jsonbs, err := json.Marshal(resp)
	if err != nil {
		t.Error(err.Error())
		return
	}

	t.Logf("%s", jsonbs)
}
