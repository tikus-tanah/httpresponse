package httpresponse

import (
	"fmt"
	"reflect"
	"unicode"
)

type Response interface{}

type Field struct {
	Name      string
	Value     interface{}
	Omitempty bool
}

type errorMsg struct {
	Type    string `json:"type,omitempty"`
	Message string `json:"message,omitempty"`
}

func Continue() Response {
	return makeStruct(StatusContinue)
}

func SwitchingProtocols() Response {
	return makeStruct(StatusSwitchingProtocols)
}

func MakeResponse(status []Field, fields ...Field) Response {
	var fs []Field
	if status != nil {
		fs = append(fs, status...)
	}

	fs = append(fs, fields...)

	return makeStruct(fs)
}

func OK(value interface{}, vname string) Response {
	fs := StatusOK
	if vname != "" {
		fs = append(fs, Field{
			Name:  vname,
			Value: value,
		})
	}

	return makeStruct(fs)
}

func Created(value interface{}, vname string) Response {
	fs := StatusCreated
	if vname != "" {
		fs = append(fs, Field{
			Name:  vname,
			Value: value,
		})
	}

	return makeStruct(fs)
}

func Accepted(value interface{}, vname string) Response {
	fs := StatusAccepted
	if vname != "" {
		fs = append(fs, Field{
			Name:  vname,
			Value: value,
		})
	}

	return makeStruct(fs)
}

func NonAuthoritativeInformation(value interface{}, vname string) Response {
	fs := StatusNonAuthoritativeInformation
	if vname != "" {
		fs = append(fs, Field{
			Name:  vname,
			Value: value,
		})
	}

	return makeStruct(fs)
}

func NoContent(value interface{}, vname string) Response {
	fs := StatusNoContent
	if vname != "" {
		fs = append(fs, Field{
			Name:  vname,
			Value: value,
		})
	}

	return makeStruct(fs)
}

func ResetContent(value interface{}, vname string) Response {
	fs := StatusResetContent
	if vname != "" {
		fs = append(fs, Field{
			Name:  vname,
			Value: value,
		})
	}

	return makeStruct(fs)
}

func PartialContent(value interface{}, vname string) Response {
	fs := StatusPartialContent
	if vname != "" {
		fs = append(fs, Field{
			Name:  vname,
			Value: value,
		})
	}

	return makeStruct(fs)
}

func MultipleChoices() Response {
	return makeStruct(StatusMultipleChoices)
}

func MovedPermanently() Response {
	return makeStruct(StatusMovedPermanently)
}

func Found() Response {
	return makeStruct(StatusFound)
}

func SeeOther() Response {
	return makeStruct(StatusSeeOther)
}

func NotModified() Response {
	return makeStruct(StatusNotModified)
}

func UseProxy() Response {
	return makeStruct(StatusUseProxy)
}

func TemporaryRedirect() Response {
	return makeStruct(StatusTemporaryRedirect)
}

func BadRequest(errType, errMsg string) Response {
	fs := StatusBadRequest
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func Unauthorized(errType, errMsg string) Response {
	fs := StatusUnauthorized
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func PaymentRequired(errType, errMsg string) Response {
	fs := StatusPaymentRequired
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func Forbidden(errType, errMsg string) Response {
	fs := StatusForbidden
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func NotFound(errType, errMsg string) Response {
	fs := StatusNotFound
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func MethodNotAllowed(errType, errMsg string) Response {
	fs := StatusMethodNotAllowed
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func NotAcceptable(errType, errMsg string) Response {
	fs := StatusNotAcceptable
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func ProxyAuthenticationRequired(errType, errMsg string) Response {
	fs := StatusProxyAuthenticationRequired
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func RequestTimeout(errType, errMsg string) Response {
	fs := StatusRequestTimeout
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func Conflict(errType, errMsg string) Response {
	fs := StatusConflict
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func Gone(errType, errMsg string) Response {
	fs := StatusGone
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func LengthRequired(errType, errMsg string) Response {
	fs := StatusLengthRequired
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func PreconditionFailed(errType, errMsg string) Response {
	fs := StatusPreconditionFailed
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func RequestEntityTooLarge(errType, errMsg string) Response {
	fs := StatusRequestEntityTooLarge
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func RequestURITooLong(errType, errMsg string) Response {
	fs := StatusRequestURITooLong
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func UnsupportedMediaType(errType, errMsg string) Response {
	fs := StatusUnsupportedMediaType
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func RequestedRangeNotSatisfiable(errType, errMsg string) Response {
	fs := StatusRequestedRangeNotSatisfiable
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func ExpectationFailed(errType, errMsg string) Response {
	fs := StatusExpectationFailed
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func InternalServerError(errType, errMsg string) Response {
	fs := StatusInternalServerError
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func NotImplemented(errType, errMsg string) Response {
	fs := StatusNotImplemented
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func BadGateway(errType, errMsg string) Response {
	fs := StatusBadGateway
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func ServiceUnavailable(errType, errMsg string) Response {
	fs := StatusServiceUnavailable
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func GatewayTimeout(errType, errMsg string) Response {
	fs := StatusGatewayTimeout
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func HTTPVersionNotSupported(errType, errMsg string) Response {
	fs := StatusHTTPVersionNotSupported
	if errType != "" || errMsg != "" {
		fs = append(fs, Field{
			Name: "error",
			Value: errorMsg{
				Type:    errType,
				Message: errMsg,
			},
		})
	}

	return makeStruct(fs)
}

func makeStruct(fs []Field) Response {
	rfs := make([]reflect.StructField, 0)
	vs := make([]reflect.Value, 0)
	for _, f := range fs {
		v := reflect.ValueOf(f.Value)
		for v.Kind() == reflect.Interface {
			v = v.Elem()
		}

		// skip if value is invalid
		if !v.IsValid() {
			continue
		}

		fname := []byte(f.Name)
		fname[0] = byte(unicode.ToUpper(rune(fname[0])))

		vs = append(vs, v)
		rf := reflect.StructField{
			Name: string(fname),
			Type: v.Type(),
			Tag:  reflect.StructTag(fmt.Sprintf(`json:"%s"`, f.Name)),
		}

		if f.Omitempty {
			rf.Tag = reflect.StructTag(fmt.Sprintf(`json:"%s,omitempty"`, f.Name))
		}

		rfs = append(rfs, rf)
	}

	stct := reflect.New(reflect.StructOf(rfs)).Elem()
	for i, v := range vs {
		stct.Field(i).Set(v)
	}

	return stct.Addr().Interface()
}
