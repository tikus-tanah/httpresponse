# httpresponse - HTTP Response builder for easy response creation

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/tikus-tanah/httpresponse.svg)](https://pkg.go.dev/gitlab.com/tikus-tanah/httpresponse)

httpresponse is a lightweight library for easy HTTP response creation with pre-defined response maker functions

## Install
```
go get -u gitlab.com/tikus-tanah/httpresponse
```

## Examples

### HTTP 200 OK
```go
resp := httpresponse.OK(data, dataName)
```

### HTTP 301 Moved Permanently
```go
resp := httpresponse.MovedPermanently()
```

### HTTP 400 Bad Request
```go
resp := httpresponse.BadRequest(errTyp, errMsg)
```

### Simple Server Using Gin
```go
package main

import (
	"gitlab.com/tikus-tanah/httpresponse"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.GET("/message", func(c *gin.Context) {
		name := c.Query("name")
		if name == "" {
			c.JSON(400, httpresponse.BadRequest("name_is_empty", "Name can not be empty"))
			return
		}

		c.JSON(200, httpresponse.OK("Hello "+name, "message"))
	})

	router.Run(":8000")
}
```

## License
MIT