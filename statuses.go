package httpresponse

var (
	StatusContinue = []Field{
		{
			Name:  "status",
			Value: "continue",
		},
		{
			Name:  "code",
			Value: 100,
		},
	}

	StatusSwitchingProtocols = []Field{
		{
			Name:  "status",
			Value: "switching_protocols",
		},
		{
			Name:  "code",
			Value: 101,
		},
	}

	StatusOK = []Field{
		{
			Name:  "status",
			Value: "ok",
		},
		{
			Name:  "code",
			Value: 200,
		},
	}

	StatusCreated = []Field{
		{
			Name:  "status",
			Value: "created",
		},
		{
			Name:  "code",
			Value: 201,
		},
	}

	StatusAccepted = []Field{
		{
			Name:  "status",
			Value: "accepted",
		},
		{
			Name:  "code",
			Value: 202,
		},
	}

	StatusNonAuthoritativeInformation = []Field{
		{
			Name:  "status",
			Value: "non_authoritative_information",
		},
		{
			Name:  "code",
			Value: 203,
		},
	}

	StatusNoContent = []Field{
		{
			Name:  "status",
			Value: "no_content",
		},
		{
			Name:  "code",
			Value: 204,
		},
	}

	StatusResetContent = []Field{
		{
			Name:  "status",
			Value: "reset_content",
		},
		{
			Name:  "code",
			Value: 205,
		},
	}

	StatusPartialContent = []Field{
		{
			Name:  "status",
			Value: "partial_content",
		},
		{
			Name:  "code",
			Value: 206,
		},
	}

	StatusMultipleChoices = []Field{
		{
			Name:  "status",
			Value: "multiple_choices",
		},
		{
			Name:  "code",
			Value: 300,
		},
	}

	StatusMovedPermanently = []Field{
		{
			Name:  "status",
			Value: "moved_permanently",
		},
		{
			Name:  "code",
			Value: 301,
		},
	}

	StatusFound = []Field{
		{
			Name:  "status",
			Value: "found",
		},
		{
			Name:  "code",
			Value: 302,
		},
	}

	StatusSeeOther = []Field{
		{
			Name:  "status",
			Value: "see_other",
		},
		{
			Name:  "code",
			Value: 303,
		},
	}

	StatusNotModified = []Field{
		{
			Name:  "status",
			Value: "not_modified",
		},
		{
			Name:  "code",
			Value: 304,
		},
	}

	StatusUseProxy = []Field{
		{
			Name:  "status",
			Value: "use_proxy",
		},
		{
			Name:  "code",
			Value: 305,
		},
	}

	StatusTemporaryRedirect = []Field{
		{
			Name:  "status",
			Value: "temporary_redirect",
		},
		{
			Name:  "code",
			Value: 307,
		},
	}

	StatusBadRequest = []Field{
		{
			Name:  "status",
			Value: "bad_request",
		},
		{
			Name:  "code",
			Value: 400,
		},
	}

	StatusUnauthorized = []Field{
		{
			Name:  "status",
			Value: "unauthorized",
		},
		{
			Name:  "code",
			Value: 401,
		},
	}

	StatusPaymentRequired = []Field{
		{
			Name:  "status",
			Value: "payment_required",
		},
		{
			Name:  "code",
			Value: 402,
		},
	}

	StatusForbidden = []Field{
		{
			Name:  "status",
			Value: "forbidden",
		},
		{
			Name:  "code",
			Value: 403,
		},
	}

	StatusNotFound = []Field{
		{
			Name:  "status",
			Value: "not_found",
		},
		{
			Name:  "code",
			Value: 404,
		},
	}

	StatusMethodNotAllowed = []Field{
		{
			Name:  "status",
			Value: "method_not_allowed",
		},
		{
			Name:  "code",
			Value: 405,
		},
	}

	StatusNotAcceptable = []Field{
		{
			Name:  "status",
			Value: "not_acceptable",
		},
		{
			Name:  "code",
			Value: 406,
		},
	}

	StatusProxyAuthenticationRequired = []Field{
		{
			Name:  "status",
			Value: "proxy_authentication_required",
		},
		{
			Name:  "code",
			Value: 407,
		},
	}

	StatusRequestTimeout = []Field{
		{
			Name:  "status",
			Value: "request_timeout",
		},
		{
			Name:  "code",
			Value: 408,
		},
	}

	StatusConflict = []Field{
		{
			Name:  "status",
			Value: "conflict",
		},
		{
			Name:  "code",
			Value: 409,
		},
	}

	StatusGone = []Field{
		{
			Name:  "status",
			Value: "gone",
		},
		{
			Name:  "code",
			Value: 410,
		},
	}

	StatusLengthRequired = []Field{
		{
			Name:  "status",
			Value: "length_required",
		},
		{
			Name:  "code",
			Value: 411,
		},
	}

	StatusPreconditionFailed = []Field{
		{
			Name:  "status",
			Value: "precondition_failed",
		},
		{
			Name:  "code",
			Value: 412,
		},
	}

	StatusRequestEntityTooLarge = []Field{
		{
			Name:  "status",
			Value: "request_entity_too_large",
		},
		{
			Name:  "code",
			Value: 413,
		},
	}

	StatusRequestURITooLong = []Field{
		{
			Name:  "status",
			Value: "request_uri_too_long",
		},
		{
			Name:  "code",
			Value: 414,
		},
	}

	StatusUnsupportedMediaType = []Field{
		{
			Name:  "status",
			Value: "unsupported_media_type",
		},
		{
			Name:  "code",
			Value: 415,
		},
	}

	StatusRequestedRangeNotSatisfiable = []Field{
		{
			Name:  "status",
			Value: "requested_range_not_satisfiable",
		},
		{
			Name:  "code",
			Value: 416,
		},
	}

	StatusExpectationFailed = []Field{
		{
			Name:  "status",
			Value: "expectation_failed",
		},
		{
			Name:  "code",
			Value: 417,
		},
	}

	StatusInternalServerError = []Field{
		{
			Name:  "status",
			Value: "internal_server_error",
		},
		{
			Name:  "code",
			Value: 500,
		},
	}

	StatusNotImplemented = []Field{
		{
			Name:  "status",
			Value: "not_implemented",
		},
		{
			Name:  "code",
			Value: 501,
		},
	}

	StatusBadGateway = []Field{
		{
			Name:  "status",
			Value: "bad_gateway",
		},
		{
			Name:  "code",
			Value: 502,
		},
	}

	StatusServiceUnavailable = []Field{
		{
			Name:  "status",
			Value: "service_unavailable",
		},
		{
			Name:  "code",
			Value: 503,
		},
	}

	StatusGatewayTimeout = []Field{
		{
			Name:  "status",
			Value: "gateway_timeout",
		},
		{
			Name:  "code",
			Value: 504,
		},
	}

	StatusHTTPVersionNotSupported = []Field{
		{
			Name:  "status",
			Value: "http_version_not_supported",
		},
		{
			Name:  "code",
			Value: 505,
		},
	}
)
